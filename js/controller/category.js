
Vue.component('category-create-view', function (resolve) {
    axios.get('templates/' + in_temp)
        .then(onSucces)
        .catch(function (error) {
        });
    function onSucces(resposen) {
        resolve({
            data: function () {
                app.$data.status_arr= [
                    'enable',
                    'delete',
                    'disable'
                ]
                return app.$data;
            },

            methods: {
                save: function (event) {
                    postsArr = app.$data
                    postsArr = JSON.parse(JSON.stringify(postsArr));
                    delete (postsArr.authors)
                    delete (postsArr.categorys)
                    delete (postsArr.deployed)
                    delete (postsArr.html_made)
                    delete (postsArr.status_arr)
                    axios.defaults.headers.post['session'] = session;

                    axios.post(base_url + '/api/category', postsArr)
                        .then((res) => {
                            //導頁
                            location.href = '?action=article_list';
                        }).catch((error) => {
                            if(error.response.data.code == 410){
                                app.sessioninit();
                                alert('時間過期，請重新整理頁面');
                            }else{
                                alert(error.response.data.comment);
                            }
                        });
                },
                change: function (e) {
                    var files = e.target.files || e.dataTransfer.files;
                    if (!files.length)
                        return;

                    var formData = new FormData();
                    formData.append("file", files[0]);
                    formData.append("parent_type", 'category');
                    axios.defaults.headers.post['session'] = session;
                    axios.post(base_url + '/api/file', formData)
                        .then((res) => {
                            this.cover = res.data.data.filename;
                        }).catch(function (error) {
                        });
                }
            },
            template: resposen.data
        });
    }
})

Vue.component('category-edit-view', function (resolve) {
    var slug = null;
    axios.get('templates/' + in_temp)
        .then(onSucces)
        .catch(function (error) {
        });
    function onSucces(resposen) {
        resolve({
            data: function () {
                app.$data.status_arr= [
                    'enable',
                    'delete',
                    'disable'
                ]
                return app.$data;
            },
            //get sub component
            created: function () {
                category_id = id;
                axios.get(base_url + '/api/category/' + category_id, { headers: { 'session': session } })
                    .then((res) => {
                        postData = res.data.data.category;
                        this.name = postData.name;
                        this.description = postData.description;
                        this.content = postData.content;
                        this.cover_title = postData.cover_title;
                        this.status = postData.status;
                        this.author_id = postData.author_id;
                        this.category_id = postData.category_id;
                        this.slug = postData.slug;
                        slug = postData.slug;
                        this.excerpt = postData.excerpt;
                        this.og_title = postData.og_title;
                        this.og_description = postData.og_description;
                        this.meta_title = postData.meta_title;
                        this.meta_description = postData.meta_description;
                        this.cover_title = postData.cover_title;
                        this.cover_alt = postData.cover_alt;
                        if (postData.html_made == '0') {
                            this.html_made = '無';
                        } else {
                            this.html_made = '有';
                        }
                        if (postData.synced == '0') {
                            this.synced = '無';
                        } else {
                            this.synced = '有';
                        }

                    });
            },
            //end sub component
            template: resposen.data,

            methods: {
                save: function (event) {
                    postsArr = app.$data

                    postsArr = JSON.parse(JSON.stringify(postsArr));
                    if (slug == postsArr.slug) {
                        delete (postsArr.slug)
                    }
                    delete (postsArr.authors)
                    delete (postsArr.categorys)
                    delete (postsArr.deployed)
                    delete (postsArr.html_made)
                    delete (postsArr.status_arr)
                    category_id = id;
                    axios.defaults.headers.put['session'] = session;
                    axios.put(base_url + '/api/category/' + category_id, postsArr)
                        .then((res) => {
                        }).catch((error) => {
                            if(error.response.data.code == 410){
                                app.sessioninit();
                                alert('時間過期，請重新整理頁面');
                            }else{
                                alert(error.response.data.comment);
                            }
                        });
                },

                sync: function (event) {
                    post_id = id;
                    axios.defaults.headers.put['session'] = session;
                    axios.put(base_url + '/api/category/' + post_id + '/html/sync')
                        .then((res) => {

                        }).catch((error) => {
                            if(error.response.data.code == 410){
                                app.sessioninit();
                                alert('時間過期，請重新整理頁面');
                            }else{
                                alert(error.response.data.comment);
                            }
                        });
                },
                make2html: function (event) {
                    post_id = id;
                    axios.defaults.headers.put['session'] = session;
                    axios.put(base_url + '/api/category/' + post_id + '/html/make')
                        .then((res) => {

                        }).catch((error) => {
                            if(error.response.data.code == 410){
                                app.sessioninit();
                                alert('時間過期，請重新整理頁面');
                            }else{
                                alert(error.response.data.comment);
                            }
                        });
                },

                change: function (e) {
                    var files = e.target.files || e.dataTransfer.files;
                    if (!files.length)
                        return;

                    var formData = new FormData();
                    formData.append("file", files[0]);
                    axios.defaults.headers.post['session'] = session;
                    axios.post(base_url + '/api/file', formData)
                        .then((res) => {
                            this.cover = res.data.data.filename;
                        }).catch(function (error) {

                        });
                }
            },
        });
    }
})

Vue.component('category-list-view', function (resolve) {
    axios.get('templates/' + in_temp)
        .then(onSucces)
        .catch(function (error) {

        });
    function onSucces(resposen) {
        resolve({
            data: function () {
                app.$data.status_arr= [
                    'enable',
                    'delete',
                    'disable'
                ]
                return app.$data;
            },

            created: function () {
                axios.defaults.headers.get['session'] = session;
                axios.get(base_url + '/api/category/list')
                    .then((res) => {
                        postData = res.data.data.categories;
                        for(var i=0; i<postData.length; i++){
                             if (postData[i].html_made == '0') {
                                 postData[i].html_made = '無';
                             } else {
                                 postData[i].html_made = '有';
                             }
                             if (postData[i].synced == '0') {
                                 postData[i].synced = '無';
                             } else {
                                 postData[i].synced = '有';
                             }
                        }
                        this.datas = postData;
                    }).catch((error) => {
                        if(error.response.data.code == 410){
                            app.sessioninit();
                            alert('時間過期，請重新整理頁面');
                        }else{
                            alert(error.response.data.comment);
                        }
                    });
            },
            methods: {
            },
            template: resposen.data
        });
    }
})